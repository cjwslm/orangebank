Instructions:
 - Clone or download the repository
 - Compile and package the proyect with maven command to generate the jar:
		mvn clean verify
 - Into the target folder run the command to deploy the microservice: 
		java -jar test-0.0.1-SNAPSHOT.jar

Endpoints:
 - Create Transaction:
		http://localhost:8080/createTransaction

		Body: 
		{
			"reference":"12345A",
			"accountIban":"ES9820385778983000760236",
			"date":"2019-07-16T16:55:42.000Z",
			"amount":193.38,
			"fee":3.18,
			"description":"Restaurant payment"
		}

- Search Transaction:
		http://localhost:8080/searchTransactions

		Body: 
		{
			"ascending": false,
			"accountIban": "ES9820385778983000760236"
		}
		

- Transaction Status:
		http://localhost:8080/transactionStatus

		Body: 
		{
        	"reference":"12345A",
        	"channel":"CLIENT"
        }