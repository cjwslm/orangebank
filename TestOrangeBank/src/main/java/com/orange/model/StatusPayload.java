package com.orange.model;

import javax.validation.constraints.NotNull;

public class StatusPayload {
    public enum ChannelEnum {
        CLIENT, ATM, INTERNAL
    }

    @NotNull
    String reference; // "12345A"
    ChannelEnum channel;

    public StatusPayload() {
        super();
        this.reference = "";
    }

    public StatusPayload(String reference, ChannelEnum channel) {
        super();
        this.reference = reference;
        this.channel = channel;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public ChannelEnum getChannel() {
        return channel;
    }

    public void setChannel(ChannelEnum channel) {
        this.channel = channel;
    }
}
