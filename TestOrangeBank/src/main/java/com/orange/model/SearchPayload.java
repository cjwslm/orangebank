package com.orange.model;

public class SearchPayload {
    Boolean ascending;
    String accountIban;

    public SearchPayload() {
        super();
    }

    public SearchPayload(Boolean ascending, String accountIban) {
        this.ascending = ascending;
        this.accountIban = accountIban;
    }

    public String getAccountIban() {
        return accountIban;
    }

    public void setAccountIban(String accountIban) {
        this.accountIban = accountIban;
    }

    public Boolean getAscending() {
        return ascending;
    }

    public void setAscending(Boolean ascending) {
        this.ascending = ascending;
    }

}
