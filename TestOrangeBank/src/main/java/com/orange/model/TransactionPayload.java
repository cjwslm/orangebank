package com.orange.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TransactionPayload {
    // We don't use the @NotNull notation to reuse the class in searches, where
    // these fields are not mandatory

    @Id
    String reference; // "12345A"
    String accountIban; // "ES9820385778983000760236",
    String description; // "Restaurant payment"
    Date date; // "2019-07-16T16:55:42.000Z",
    Double amount; // 193.38,
    Double fee; // 3.18,

    public TransactionPayload() {
        super();
    }

    public TransactionPayload(String accountIban, String reference, String description, Date date, Double amount, Double fee) {
        this.accountIban = accountIban;
        this.reference = reference;
        this.description = description;
        this.date = date;
        this.amount = amount;
        this.fee = fee;
    }

    public String getAccountIban() {
        return accountIban;
    }

    public void setAccountIban(String accountIban) {
        this.accountIban = accountIban;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }
}
