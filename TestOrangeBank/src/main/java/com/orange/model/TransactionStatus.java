package com.orange.model;

public class TransactionStatus {
    public enum StatusEnum {
        PENDING, SETTLED, FUTURE, INVALID
    }

    String reference; // "12345A",
    StatusEnum status; // "PENDING",
    Double amount; // 193.38,
    Double fee; // 3.18

    public TransactionStatus(String reference, StatusEnum status, Double amount, Double fee) {
        this.reference = reference;
        this.status = status;
        this.amount = amount;
        this.fee = fee;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

}
