package com.orange.controller;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.orange.model.SearchPayload;
import com.orange.model.StatusPayload;
import com.orange.model.TransactionPayload;
import com.orange.model.TransactionStatus;
import com.orange.model.TransactionStatus.StatusEnum;
import com.orange.repository.TransactionRepository;
import com.orange.service.OrangeBankService;
import com.orange.service.impl.CreateTransactionImpl;
import com.orange.service.impl.SearchTransactionImpl;
import com.orange.service.impl.TransactionStatusImpl;

@Controller
public class OrangeBankController implements OrangeBankService {

    @Autowired
    CreateTransactionImpl createTransaction;

    @Autowired
    SearchTransactionImpl searchTransaction;

    @Autowired
    TransactionStatusImpl transactionStatus;

    @Autowired
    private TransactionRepository transactionRepository;

    @PostConstruct
    public void feedRepository() {
        transactionRepository.save(new TransactionPayload("ES9820385778983000760236", "12345A", "Transaction Description 1",
                new Date(System.currentTimeMillis()), 193.38d, 3.18d));
        transactionRepository.save(new TransactionPayload("ES9820385778983000760236", "12346A", "Transaction Description 2",
                new Date(System.currentTimeMillis()), 193.18d, 4.18d));
        transactionRepository.save(new TransactionPayload("ES9820385778983000760236", "12347A", "Transaction Description 3",
                new Date(System.currentTimeMillis()), 193.28d, 5.18d));
        transactionRepository.save(new TransactionPayload("ES9820385778983000760236", "12348A", "Transaction Description 4",
                new Date(System.currentTimeMillis()), 193.48d, 6.18d));
        transactionRepository.save(new TransactionPayload("ES9820385778983000760236", "12349A", "Transaction Description 5",
                new Date(System.currentTimeMillis()), 193.58d, 7.18d));
        transactionRepository.save(new TransactionPayload("ES9820385778983000760236", "12342A", "Transaction Description 6",
                new Date(System.currentTimeMillis()), 193.68d, 8.18d));
        transactionRepository.save(new TransactionPayload("ES9820385778983000760236", "12343A", "Transaction Description 7",
                new Date(System.currentTimeMillis()), 193.78d, 9.18d));
    }

    @Override
    public Boolean createTransaction(TransactionPayload input) {
        return createTransaction.createTransaction(input);
    }

    @Override
    public List<TransactionPayload> searchTransactions(SearchPayload input) {
        return searchTransaction.searchTransaction(input);
    }

    @Override
    public TransactionStatus transactionStatus(StatusPayload input) {
        return transactionStatus.transactionStatus(input);
    }

}
