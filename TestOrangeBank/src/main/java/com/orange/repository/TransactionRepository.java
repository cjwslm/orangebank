package com.orange.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orange.model.TransactionPayload;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionPayload, String> {
    public List<TransactionPayload> findAllByOrderByAmountAsc();

    public List<TransactionPayload> findAllByOrderByAmountDesc();
}