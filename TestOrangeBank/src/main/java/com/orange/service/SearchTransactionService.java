package com.orange.service;

import java.util.List;

import com.orange.model.SearchPayload;
import com.orange.model.TransactionPayload;

public interface SearchTransactionService {
    public List<TransactionPayload> searchTransaction(SearchPayload payload);
}
