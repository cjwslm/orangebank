package com.orange.service;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.orange.model.SearchPayload;
import com.orange.model.StatusPayload;
import com.orange.model.TransactionPayload;
import com.orange.model.TransactionStatus;

public interface OrangeBankService {
    @RequestMapping(value = "/createTransaction", method = RequestMethod.POST)
    public @ResponseBody Boolean createTransaction(@RequestBody TransactionPayload input);

    @RequestMapping(value = "/searchTransactions", method = RequestMethod.POST)
    public @ResponseBody List<TransactionPayload> searchTransactions(@RequestBody SearchPayload input);

    @RequestMapping(value = "/transactionStatus", method = RequestMethod.POST)
    public @ResponseBody TransactionStatus transactionStatus(@RequestBody StatusPayload input);

}
