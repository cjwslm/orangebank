package com.orange.service;

import java.util.List;

import com.orange.model.SearchPayload;
import com.orange.model.StatusPayload;
import com.orange.model.TransactionPayload;
import com.orange.model.TransactionStatus;

public interface DbTransactionService {
    public Boolean saveTransaction(TransactionPayload input);

    public List<TransactionPayload> loadTransactions(SearchPayload input);

    public TransactionStatus loadTransactionStatus(StatusPayload input);
}
