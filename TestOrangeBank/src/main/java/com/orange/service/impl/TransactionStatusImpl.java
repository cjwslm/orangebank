package com.orange.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orange.model.StatusPayload;
import com.orange.model.TransactionStatus;
import com.orange.service.DbTransactionService;
import com.orange.service.TransactionStatusService;

@Service
public class TransactionStatusImpl implements TransactionStatusService {

    @Autowired
    DbTransactionServiceImpl dbService;

    public TransactionStatusImpl() {
        super();
    }

    @Override
    public TransactionStatus transactionStatus(StatusPayload input) {
        return dbService.loadTransactionStatus(input);
    }

}
