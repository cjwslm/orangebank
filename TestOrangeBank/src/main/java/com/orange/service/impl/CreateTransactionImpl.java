package com.orange.service.impl;

import java.util.Date;
import java.util.UUID;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orange.model.TransactionPayload;
import com.orange.service.CreateTransactionService;

@Service
public class CreateTransactionImpl implements CreateTransactionService {

    @Autowired
    DbTransactionServiceImpl dbService;

    public CreateTransactionImpl() {
        super();
    }

    @Override
    public Boolean createTransaction(TransactionPayload input) {
        // Validate mandatory fields
        Assert.assertNotNull(input.getAccountIban());
        Assert.assertFalse(input.getAccountIban().isEmpty());
        Assert.assertNotNull(input.getAmount());

        // Validate Reference, if it is empty we fill it with a new UUID
        if (input.getReference() == null || input.getReference().isEmpty()) {
            input.setReference(UUID.randomUUID().toString());
        }

        // Validate Date field, if it is empty we fill it with the current date
        if (input.getDate() == null) {
            input.setDate(new Date(System.currentTimeMillis()));
        }

        return dbService.saveTransaction(input);
    }

}
