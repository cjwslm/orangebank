package com.orange.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orange.model.SearchPayload;
import com.orange.model.StatusPayload;
import com.orange.model.StatusPayload.ChannelEnum;
import com.orange.model.TransactionPayload;
import com.orange.model.TransactionStatus;
import com.orange.model.TransactionStatus.StatusEnum;
import com.orange.repository.TransactionRepository;
import com.orange.service.DbTransactionService;

@Service
public class DbTransactionServiceImpl implements DbTransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public Boolean saveTransaction(TransactionPayload input) {
        transactionRepository.save(input);
        return true;
    }

    @Override
    public List<TransactionPayload> loadTransactions(SearchPayload input) {
        if (input.getAscending()) {
            return transactionRepository.findAllByOrderByAmountAsc();
        }
        return transactionRepository.findAllByOrderByAmountDesc();
    }

    @Override
    public TransactionStatus loadTransactionStatus(StatusPayload input) {

        List<TransactionPayload> resultFromDDBB = transactionRepository.findAll();

        if (resultFromDDBB != null && !resultFromDDBB.isEmpty() && input.getChannel() != null) {
            for (TransactionPayload tr : resultFromDDBB) {
                int option = tr.getDate().compareTo(new Date(System.currentTimeMillis()));
                if (input.getChannel() == ChannelEnum.ATM) {
                    return new TransactionStatus(tr.getReference(), getOptionAtm(option), tr.getAmount() - tr.getFee(), null);
                } else if (input.getChannel() == ChannelEnum.CLIENT) {
                    return new TransactionStatus(tr.getReference(), getOptionClientInternal(option), tr.getAmount() - tr.getFee(), null);
                } else if (input.getChannel() == ChannelEnum.INTERNAL) {
                    return new TransactionStatus(tr.getReference(), getOptionClientInternal(option), tr.getAmount(), tr.getFee());
                }
            }
        }
        return new TransactionStatus("XXXXXX", StatusEnum.INVALID, null, null);
    }

    private StatusEnum getOptionAtm(int option) {
        return option < 0 ? StatusEnum.SETTLED : StatusEnum.PENDING;
    }

    // The ternary condition is not used for quality issues of the Sonar code
    private StatusEnum getOptionClientInternal(int option) {
        if (option < 0) {
            return StatusEnum.SETTLED;
        } else if (option == 0) {
            return StatusEnum.PENDING;
        }
        return StatusEnum.FUTURE;
    }

}
