package com.orange.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orange.model.SearchPayload;
import com.orange.model.TransactionPayload;
import com.orange.service.DbTransactionService;
import com.orange.service.SearchTransactionService;

@Service
public class SearchTransactionImpl implements SearchTransactionService {

    @Autowired
    DbTransactionServiceImpl dbService;

    public SearchTransactionImpl() {
        super();
    }

    @Override
    public List<TransactionPayload> searchTransaction(SearchPayload input) {
        return dbService.loadTransactions(input);
    }

}
