package com.orange.service;

import com.orange.model.TransactionPayload;

public interface CreateTransactionService {
    public Boolean createTransaction(TransactionPayload payload);
}
