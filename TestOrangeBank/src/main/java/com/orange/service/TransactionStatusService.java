package com.orange.service;

import com.orange.model.StatusPayload;
import com.orange.model.TransactionStatus;

public interface TransactionStatusService {
    public TransactionStatus transactionStatus(StatusPayload input);
}
