package com.orange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.orange"})
public class TestOrangeBankApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestOrangeBankApplication.class, args);
	}

}