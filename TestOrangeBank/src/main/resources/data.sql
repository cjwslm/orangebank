DROP TABLE IF EXISTS transactions;
 
CREATE TABLE transactions (
	reference VARCHAR(250) NOT NULL,    
	accountIban VARCHAR(250) NOT NULL,
	description VARCHAR(250),
	date DATETIME,
	amount DOUBLE NOT NULL,
	fee DOUBLE,
	CONSTRAINT reference_id PRIMARY KEY (reference)
);
 
INSERT INTO transactions (accountIban, reference, description, date, amount, fee) VALUES
  ('ES9820385778983000760236', '12345A', 'Transaction Description', '2019-07-16T16:55:42.000Z', 193.38, 3.18),
  ('ES9820385778983000760237', '12346A', 'Transaction Description', '2019-07-18T15:15:44.000Z', 193.38, 4.20),
  ('ES9820385778983000760238', '12347A', 'Transaction Description', '2019-07-15T18:45:43.000Z', 193.38, 5.10);